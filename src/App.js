import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from './Button';

class App extends Component {
  state = { val: 0 };
  update = (r) => {
    if (r == -1 && this.state.val == 0) {
      this.setState((prevState) => ({ val: prevState.val }));
    }
    else {
      this.setState((prevState) => ({ val: prevState.val + r }));
    }
  }
  render() {
    let a = 1;
    let b = -1;
    return (
          <span className="con"><Button op={"-"} handleClick={() => this.update(b)} />
              <span className="val">{this.state.val}</span> 
              <Button op={"+"} handleClick={() => this.update(a)} /></span>
    );
  }
}

export default App;
